<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas UTS</title>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
      crossorigin="anonymous"
    />
</head>
<body>
    <form method="POST" action="hasil.php">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow-sm">
  <div class="container">
    <a class="navbar-brand"><strong>Input Data</strong></a>
  </div>
</nav>
<section id="data">
      <div class="container">
        <div class="row text-center mb-3">
          <div class="col">
            <h2>Silahkan Isi Data Dibawah</h2>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-6">
            <form>
              <div class="mb-3">
                <label for="name" class="form-label"><b>Nama Lengkap</b></label>
                <input
                  type="text"
                  class="form-control"
                  name="name"
                  aria-describedby="name"
                />
              </div>
              <div class="mb-3">
                <label for="email" class="form-label"><b>Email</b></label>
                <input
                  type="email"
                  class="form-control"
                  name="email"
                  aria-describedby="email"
                />
              </div>
              <div class="mb-3">
                <label for="email" class="form-label"><b>No. Telepon</b></label>
                <input
                  type="telepon"
                  class="form-control"
                  name="telepon"
                  aria-describedby="telepon"
                />
              </div>
              <button type="submit" class="btn btn-primary mb-3">Kirim</button>
            </form>
          </div>
        </div>
      </div>
    </section>
    </form>
</body>
</html>