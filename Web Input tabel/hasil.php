<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
      crossorigin="anonymous"
    />
    <title>Document</title>
</head>
<body class="container">
    <table class="table">
        <thead class="table-light">
            <tr>
                <th scope="col" class="text-center">Nama</th>
                <th scope="col" class="text-center">Email</th>
                <th scope="col" class="text-center">No. Telepon</th>
            </tr>
        </thead>
        <tbody class="text-center">
            <tr>
                <td><?php echo $_POST['name'] ?></td>
                <td><?php echo $_POST['email'] ?></td>
                <td><?php echo $_POST['telepon'] ?></td>
            </tr>
        </tbody>
    </table>
</body>
</html>